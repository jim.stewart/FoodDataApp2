package org.thelangschool.fooddataapp;

import java.io.IOException;

public class Main {

	  public static void main(String[] args) {
		    ApiClient c = new ApiClient();
		    try {
		      Food cheddar_cheese = ApiClient.getFoodByName("cheddar%20cheese");
		      System.out.println(cheddar_cheese.toString());
		    } catch (IOException e){
		      System.out.println(e);
		    } catch (InterruptedException e){
		      System.out.println(e);
		    }
		  }

}
